gcc \
    -O2 -fPIC -Wl,--gc-sections \
    -lpthread -lX11 \
    -fno-stack-protector \
    -I../src \
    ../src/veadohook.c \
    ../src/platform_win.c \
    ../src/platform_linux.c \
    ../src/platform_mac.c \
    ../src/test.c \
    -o test
./test