#include "veadohook.h"
#ifdef PLATFORM_LINUX

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <time.h>
#include <errno.h>
#include <X11/Xlib.h>

typedef void(PROCEXPORT *eventcallback_t)(void*);

#define DEVICE_LIST_PATH "/proc/bus/input/devices"
#define INPUT_INDEX_COUNT 32
#define CALLBACK_MAX 4
#define INPUT_PATH_MAX 20
#define INPUT_DATA_MAX 8

static const char* input_path = "/dev/input/";

typedef void(*eventcallback_t)(void*);

struct inputentry_struct;

typedef struct {
	eventcallback_t callback[CALLBACK_MAX];
	uint32_t callback_count;
	int enabled;
	pthread_t thread;
	//
	int init;
	struct inputentry_struct* entry;
	pthread_mutex_t mutex;
	char path[INPUT_PATH_MAX];
} inputindex_t;

typedef struct inputentry_struct {
	const char* name;
	const ssize_t event_size;
	inputindex_t index[INPUT_INDEX_COUNT + 1];
} inputentry_t;

#define INPUT_ENTRY_INIT(x, y) { x, sizeof(y), {0} }

void input_lock(inputindex_t* i) {
	pthread_mutex_lock(&i->mutex);
}

void input_unlock(inputindex_t* i) {
	pthread_mutex_unlock(&i->mutex);
}

void sleep_ms(uint32_t ms) {
	struct timespec ts = { ms / 1000, (ms % 1000) * 1000000L };
	nanosleep(&ts, NULL);
}

void* input_threadproc(void* ptr) {
	inputindex_t* i = ptr;
	printf("(hi from the %s thread)\n", i->path);
	ssize_t size = i->entry->event_size;
	uint8_t data[INPUT_DATA_MAX];
	while (1) {
		int fd = open(i->path, O_RDONLY | O_NONBLOCK);
		if (fd > 0) {
			printf("(opened %s for reading)\n", i->path);
			int sleep_count = 0;
			while (1) {
				int r = read(fd, data, size);
				if (r < 0 && errno != EAGAIN && errno != EWOULDBLOCK) break;
				if (r == size) {
					input_lock(i);
					for (uint32_t a = 0; a < i->callback_count; a++) {
						i->callback[a](data);
					}
					input_unlock(i);
					sleep_count++;
					if (sleep_count < 64) continue;
				}
				sleep_count = 0;
				sleep_ms(1);
				if (!i->enabled) break;
			}
			close(fd);
			printf("(closed %s)\n", i->path);
		} else {
			printf("(coudln't open %s. trying again soon...)\n", i->path);
		}
		if (!i->enabled) break;
		int stop = 0;
		for (int a = 0; a < 10; a++) {
			sleep_ms(100);
			if (!i->enabled) {
				stop = 1;
				break;
			}
		}
		if (stop) break;
	}
	printf("(bye from the %s thread)\n", i->path);
	return NULL;
}

void input_set(int enable, inputentry_t* entry, int32_t index, eventcallback_t callback) {
	if (index < -1 || index >= INPUT_INDEX_COUNT) return;
	inputindex_t* i = (entry->index + index + 1);
	if (!i->init) {
		i->init = 1;
		i->entry = entry;
		pthread_mutex_init(&i->mutex, NULL);
		memset(i->path, '\0', sizeof(i->path));
		uint32_t si = 0;
		for (uint32_t a = 0; input_path[a] != '\0'; a++, si++) {
			i->path[si] = input_path[a];
		}
		for (uint32_t a = 0; entry->name[a] != '\0'; a++, si++) {
			i->path[si] = entry->name[a];
		}
		if (index >= 0) sprintf(i->path + si, "%d", index);
	}
	if (enable) {
		input_lock(i);
		if (i->callback_count < CALLBACK_MAX) {
			i->callback[i->callback_count] = callback;
			i->callback_count++;
			if (!i->enabled) {
				i->enabled = 1;
				i->entry = entry;
				int err = pthread_create(&(i->thread), NULL, input_threadproc, i);
				if (err) {
					i->enabled = 0;
					printf("thread creation failed: %d\n", err);
				}
			}
		}
		input_unlock(i);
	} else {
		int join = 0;
		input_lock(i);
		if (i->callback_count > 0) {
			uint32_t a = i->callback_count - 1;
			int found = 0;
			while (1) {
				if (i->callback[a] == callback) {
					found = 1;
					break;
				}
				if (a == 0) break;
				a--;
			}
			if (found) {
				i->callback_count--;
				for (; a < i->callback_count; a++) {
					i->callback[a] = i->callback[a + 1];
				}
				if (i->callback_count == 0 && i->enabled) {
					i->enabled = 0;
					join = 1;
				}
			}
		}
		input_unlock(i);
		if (join) pthread_join(i->thread, NULL);
	}
}



//

typedef struct input_event input_event_t;

uint32_t hook_flags = 0;
pthread_t hook_thread;
pthread_mutex_t hook_thread_lock = PTHREAD_MUTEX_INITIALIZER;

inputentry_t input_event = INPUT_ENTRY_INIT("event", input_event_t);

void mouse_callback(void* data) {
	input_event_t* e = data;
	if (e->type != EV_KEY) return;
	hook_mouse_t mouse;
	switch (e->code) {

	case BTN_LEFT:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_LBUTTON_DOWN : HOOK_MOUSE_EVENT_LBUTTON_UP;
		break;

	case BTN_RIGHT:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_RBUTTON_DOWN : HOOK_MOUSE_EVENT_RBUTTON_UP;
		break;

	case BTN_MIDDLE:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_MBUTTON_DOWN : HOOK_MOUSE_EVENT_MBUTTON_UP;
		break;

	case BTN_SIDE:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_XBUTTON_DOWN : HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse.xbutton = 1;
		break;

	case BTN_EXTRA:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_XBUTTON_DOWN : HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse.xbutton = 2;
		break;

	case BTN_FORWARD:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_XBUTTON_DOWN : HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse.xbutton = 3;
		break;

	case BTN_BACK:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_XBUTTON_DOWN : HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse.xbutton = 4;
		break;

	case BTN_TASK:
		mouse.event_type = e->value ? HOOK_MOUSE_EVENT_XBUTTON_DOWN : HOOK_MOUSE_EVENT_XBUTTON_UP;
		mouse.xbutton = 5;
		break;

	default: return;
	}
	veadohook_emit(HOOK_MOUSE, &mouse);
}

void keyboard_callback(void* data) {
	input_event_t* e = data;
	if (e->type != EV_KEY) return;
	hook_keyboard_t keyboard;
	keyboard.event_type = e->value ? HOOK_KEYBOARD_EVENT_KEY_DOWN : HOOK_KEYBOARD_EVENT_KEY_UP;
	keyboard.key = e->code;
	veadohook_emit(HOOK_KEYBOARD, &keyboard);
}

void* hook_threadproc(void* ptr) {
	printf("(hi from the linux thread)\n");
	uint32_t flags_old = 0;

	uint32_t input_mouse = 0;
	uint32_t input_keyboard = 0;
	uint32_t file_check = 0;
	char line[1024];
	char handler[24];

	Display* display = NULL;
	hook_cursor_t cursor_prev = {0};

	while (1) {
		pthread_mutex_lock(&hook_thread_lock);
		uint32_t flags = hook_flags;
		pthread_mutex_unlock(&hook_thread_lock);
		int update = flags_old != flags;
		flags_old = flags;
		int emit_mouse = flags & (1 << HOOK_MOUSE);
		int emit_keyboard = flags & (1 << HOOK_KEYBOARD);
		int emit_cursor = flags & (1 << HOOK_CURSOR);

		if (update || file_check == 0) {
			file_check = 100;
			uint32_t input_mouse_new = 0;
			uint32_t input_keyboard_new = 0;
			if (emit_mouse || emit_keyboard) {
				FILE* file = fopen(DEVICE_LIST_PATH, "r");
				if (file) {
					while (fgets(line, sizeof(line), file)) {
						if (line[0] == 'H') {
							int event_code = -1;
							int is_mouse = 0;
							int is_keyboard = 0;
							uint32_t i = 12;
							while (1) {
								if (line[i] == ' ' || line[i] == '\0' || line[i] == '\n' || line[i] == '\r') break;
								uint32_t len;
								for (len = 0; line[i] != ' '; i++, len++) {
									handler[len] = line[i];
								}
								//handler[len] = '\0';
								i++;
								if (event_code < 0 && len > 5 && !strncmp(handler, "event", 5)) {
									int code = 0;
									for (int a = 5; a < len; a++) {
										char c = handler[a];
										if (c < '0' || c > '9') {
											code = -1;
											break;
										}
										code = code * 10 + (c - '0');
									}
									if (code >= 0 && code < INPUT_INDEX_COUNT) event_code = code;
								}
								if (emit_mouse && !is_mouse && len > 5 && !strncmp(handler, "mouse", 5)) is_mouse = 1;
								if (emit_keyboard && !is_keyboard && len == 3 && !strncmp(handler, "kbd", 3)) is_keyboard = 1;
							}
							if (event_code >= 0) {
								if (is_mouse) input_mouse_new |= 1 << event_code;
								if (is_keyboard) input_keyboard_new |= 1 << event_code;
							}
						}
					}
					fclose(file);
				}
			}
			if (input_mouse != input_mouse_new) {
				uint32_t keep = input_mouse & input_mouse_new;
				uint32_t new = (input_mouse | input_mouse_new) - input_mouse;
				for (uint32_t a = 0; a < 32; a++) {
					uint32_t f = 1 << a;
					if (keep & f) continue;
					input_set(new & f, &input_event, a, mouse_callback);
				}
				input_mouse = input_mouse_new;
			}
			if (input_keyboard != input_keyboard_new) {
				uint32_t keep = input_keyboard & input_keyboard_new;
				uint32_t new = (input_keyboard | input_keyboard_new) - input_keyboard;
				for (uint32_t a = 0; a < 32; a++) {
					uint32_t f = 1 << a;
					if (keep & f) continue;
					input_set(new & f, &input_event, a, keyboard_callback);
				}
				input_keyboard = input_keyboard_new;
			}
		}
		file_check--;

		if (emit_cursor) {
			hook_cursor_t cursor;
			cursor.x = 0;
			cursor.y = 0;
			cursor.width = 0;
			cursor.height = 0;
			if (!display) display = XOpenDisplay(NULL);
			if (display) {
				int n = XScreenCount(display);
				for (int a = 0; a < n; a++) {
					Window window_returned;
					int root_x, root_y;
					int win_x, win_y;
					unsigned int mask_return;
					if (XQueryPointer(display, XRootWindow(display, a), &window_returned, &window_returned, &root_x, &root_y, &win_x, &win_y, &mask_return)) {
						if (root_x < 0) root_x = 0;
						if (root_y < 0) root_y = 0;
						cursor.x = root_x;
						cursor.y = root_y;
						cursor.width = XDisplayWidth(display, a);
						cursor.height = XDisplayHeight(display, a);
						break;
					}
				}
			}
			if (cursor_prev.x != cursor.x || cursor_prev.y != cursor.y || cursor_prev.width != cursor.width || cursor_prev.height != cursor.height) {
				cursor_prev = cursor;
				veadohook_emit(HOOK_CURSOR, &cursor);
			}
		} else {
			if (display) {
				XCloseDisplay(display);
				display = NULL;
			}
		}

		if (!flags) break;
		sleep_ms(10);
	}
	printf("(bye from the linux thread)\n");
	return NULL;
}

int32_t hook(int32_t enable, int32_t code) {
	uint32_t flag = 1 << code;
	if (enable) {
		pthread_mutex_lock(&hook_thread_lock);
		uint32_t flags = hook_flags | flag;
		int create;
		if (hook_flags != flags) {
			create = !hook_flags;
			hook_flags = flags;
		} else {
			create = 0;
		}
		pthread_mutex_unlock(&hook_thread_lock);
		if (create) {
			// todo: something with polkit?
			int err = pthread_create(&hook_thread, NULL, hook_threadproc, NULL);
			if (err) {
				hook_flags = 0;
				return 0;
			}
		}
	} else {
		pthread_mutex_lock(&hook_thread_lock);
		uint32_t flags = hook_flags & ~flag;
		int destroy;
		if (hook_flags != flags) {
			hook_flags = flags;
			destroy = !hook_flags;
		} else {
			destroy = 0;
		}
		pthread_mutex_unlock(&hook_thread_lock);
		if (destroy) pthread_join(hook_thread, NULL);
	}
	return 1;
}

int32_t mouse(int32_t enable) {
	return hook(enable, HOOK_MOUSE);
}

int32_t keyboard(int32_t enable) {
	return hook(enable, HOOK_KEYBOARD);
}

int32_t cursor(int32_t enable) {
	return hook(enable, HOOK_CURSOR);
}

int32_t keyname(uint32_t key, wchar_t* str, int32_t capacity) {
	// todo: look into how to get key display names
	return 0;
}

void veadohook_platform_setup() {
	hookapi_proc[HOOK_MOUSE] = mouse;
	hookapi_proc[HOOK_KEYBOARD] = keyboard;
	hookapi_proc[HOOK_CURSOR] = cursor;
	hookapi_keysource = HOOK_KEYBOARD_SOURCE_LINUX_INPUT;
	hookapi_keyname_proc = keyname;
}

#endif
