#include "veadohook.h"
#ifdef PLATFORM_MAC

#include <CoreFoundation/CoreFoundation.h>
#include <Carbon/Carbon.h>
#include <ApplicationServices/ApplicationServices.h>
#include <stdio.h>

typedef struct {
	uint32_t count;
	CFMachPortRef tap;
	CFRunLoopSourceRef loop;
} event_tap;

event_tap mouse_data = {0};
event_tap keyboard_data = {0};

typedef void(*resetproc_t)();

hook_cursor_t cursor_prev = {0};
void cursor_reset() {
	cursor_prev = (hook_cursor_t){0};
}

#define KEYBOARD_KEYS_MAX 256
uint8_t keyboard_keys[KEYBOARD_KEYS_MAX];
void keyboard_reset() {
	memset(keyboard_keys, 0, KEYBOARD_KEYS_MAX);
}

CGEventRef mouse_callback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void* userInfo) {
	int is_cursor = 0;
	hook_mouse_t mouse;
	hook_cursor_t cursor;
	switch (type) {
		case kCGEventLeftMouseDown:
			mouse.event_type = HOOK_MOUSE_EVENT_LBUTTON_DOWN;
			break;
		case kCGEventLeftMouseUp:
			mouse.event_type = HOOK_MOUSE_EVENT_LBUTTON_UP;
			break;
		case kCGEventRightMouseDown:
			mouse.event_type = HOOK_MOUSE_EVENT_RBUTTON_DOWN;
			break;
		case kCGEventRightMouseUp:
			mouse.event_type = HOOK_MOUSE_EVENT_RBUTTON_UP;
			break;
		case kCGEventOtherMouseDown:
			mouse.event_type = HOOK_MOUSE_EVENT_MBUTTON_DOWN;
			break;
		case kCGEventOtherMouseUp:
			mouse.event_type = HOOK_MOUSE_EVENT_MBUTTON_UP;
			break;
		case kCGEventMouseMoved:
		case kCGEventLeftMouseDragged:
		case kCGEventRightMouseDragged:
		case kCGEventOtherMouseDragged:
			is_cursor = 1;
			CGPoint point = CGEventGetLocation(event);
			cursor.x = point.x <= 0 ? 0 : round(point.x);
			cursor.y = point.y <= 0 ? 0 : round(point.y);
			CGRect screenBounds = CGDisplayBounds(CGMainDisplayID());
			cursor.width = screenBounds.size.width;
			cursor.height = screenBounds.size.height;
			if (cursor.x > cursor.width - 1) cursor.x = cursor.width - 1;
			if (cursor.y > cursor.height - 1) cursor.y = cursor.height - 1;
			break;
		default: return event;
	}
	if (is_cursor) {
		if (cursor_prev.x != cursor.x || cursor_prev.y != cursor.y || cursor_prev.width != cursor.width || cursor_prev.height != cursor.height) {
			cursor_prev = cursor;
			veadohook_emit(HOOK_CURSOR, &cursor);
		}
	} else {
		veadohook_emit(HOOK_MOUSE, &mouse);
	}
	return event;
}

CGEventRef keyboard_callback(CGEventTapProxy proxy, CGEventType type, CGEventRef event, void* userInfo) {
	int emit;
	switch (type) {
		case kCGEventKeyDown: emit = 1; break;
		case kCGEventKeyUp: emit = 2; break;
		case kCGEventFlagsChanged: emit = 3; break;
		default: emit = 0; break;
	}
	if (emit > 0) {
		int64_t key = CGEventGetIntegerValueField(event, kCGKeyboardEventKeycode);
		if (key >= 0 && key < KEYBOARD_KEYS_MAX) {
			uint8_t val = keyboard_keys[key];
			uint8_t newVal = val;
			switch (emit) {
				case 1: newVal = 1; break;
				case 2: newVal = 0; break;
				case 3: newVal = val ? 0 : 1; break;
			}
			if (newVal != val) {
				keyboard_keys[key] = newVal;
				hook_keyboard_t keyboard;
				keyboard.event_type = newVal
					? HOOK_KEYBOARD_EVENT_KEY_DOWN
					: HOOK_KEYBOARD_EVENT_KEY_UP;
				keyboard.key = (uint32_t)key;
				veadohook_emit(HOOK_KEYBOARD, &keyboard);
			}
		}
	}
	return event;
}

int32_t set(int32_t enable, event_tap* data, CGEventMask mask, CGEventTapCallBack callback, resetproc_t reset) {
	if (enable) {
		if (!data->count) {
			reset();
			data->tap = CGEventTapCreate(kCGSessionEventTap, kCGTailAppendEventTap, kCGEventTapOptionListenOnly, mask, callback, NULL);
			if (data->tap == NULL) return 0;
			data->loop = CFMachPortCreateRunLoopSource(kCFAllocatorDefault, data->tap, 0);
			CFRunLoopAddSource(CFRunLoopGetCurrent(), data->loop, kCFRunLoopCommonModes);
			CGEventTapEnable(data->tap, true);
		}
		data->count++;
	} else {
		if (!data->count) return 0;
		data->count--;
		if (!data->count) {
			CGEventTapEnable(data->tap, false);
			CFRunLoopRemoveSource(CFRunLoopGetCurrent(), data->loop, kCFRunLoopCommonModes);
			data->tap = NULL;
			data->loop = NULL;
		}
	}
	return 1;
}

int32_t mouse(int32_t enable) {
	CGEventMask mask = (1 << kCGEventLeftMouseDown) | (1 << kCGEventLeftMouseUp) | (1 << kCGEventRightMouseDown) | (1 << kCGEventRightMouseUp) | (1 << kCGEventMouseMoved) | (1 << kCGEventLeftMouseDragged) | (1 << kCGEventRightMouseDragged) | (1 << kCGEventOtherMouseDragged);
	return set(enable, &mouse_data, mask, mouse_callback, cursor_reset);
}

int32_t keyboard(int32_t enable) {
	CGEventMask mask = (1 << kCGEventKeyDown) | (1 << kCGEventKeyUp) | (1 << kCGEventFlagsChanged);
	return set(enable, &keyboard_data, mask, keyboard_callback, keyboard_reset);
}

uint32_t keynamefunc(uint32_t key, vchar_t* str, uint32_t capacity) {
	TISInputSourceRef currentKeyboard = TISCopyCurrentKeyboardInputSource();
	CFDataRef layoutData = TISGetInputSourceProperty(currentKeyboard, kTISPropertyUnicodeKeyLayoutData);
	const UCKeyboardLayout *keyboardLayout = (const UCKeyboardLayout *)CFDataGetBytePtr(layoutData);
	UInt32 keysDown = 0;
	UniChar chars[64];
	UniCharCount realLength;
	UCKeyTranslate(keyboardLayout, key, kUCKeyActionDisplay, 0, LMGetKbdType(), kUCKeyTranslateNoDeadKeysBit, &keysDown, 4, &realLength, chars);
	CFRelease(currentKeyboard);
	
	uint32_t len;
	for (len = 0; len < 64 && len < realLength && len < capacity && chars[len] != '\0'; len++) {
		str[len] = chars[len];
	}
	return len;
}

void veadohook_platform_setup() {
	hookapi_proc[HOOK_MOUSE] = mouse;
	hookapi_proc[HOOK_KEYBOARD] = keyboard;
	hookapi_proc[HOOK_CURSOR] = mouse;
	hookapi_keysource = HOOK_KEYBOARD_SOURCE_MAC_EVENT_TAP;
	hookapi_keyname_proc = keynamefunc;
}

#endif
