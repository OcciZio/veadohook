# veadohook

a native plugin that grabs raw mouse & keyboard data, for use in veadotube. works on windows, linux and mac.

it's the first time i write stuff in C since college so it started out more as an experiment than anything else and it's the reason why the code isn't pretty.

but it works! mostly. here's a list of things that would be nice to look into:

* linux requires root permission to hook to mouse buttons & keyboard keys. idk how to prompt a "this app needs permissions, please type your password" type of window
* the linux cursor hook only works with X11, i think. i have no idea how it behaves under wayland
* this api could be used to grab joystick input too :] should look into that later

i'll be updating with C# examples soon.
